//
//  Project.swift
//  ltd-assing5
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Project:CustomStringConvertible
{
  
  // array containing all Persons in this Project
  private(set) var people:[Person] = []
  
  // TODO - implement all necessary methods
  
  // initializer method
  init()
  {
    people = []
  }
  
  // description method
  var description:String {
    var str:String = ""
    for person in people
    {
      str += person.description
      str += "\n"
    }
    return str
  }
  
  // add a new Person to the Project method
  func addPerson(person1:Person)
  {
    people.append(person1)
  }
  
  // sort people by age in descending order
  func sortAgeDesc() -> [Person]
  {
    let sorted = people.sorted(by: {Int($0.birthDay)! < Int($1.birthDay)!})
    return sorted
  }
  
    // sort people in the ascending order by their first names
    func sortFirstName() -> [Person]
    {
      let fnSorted = people.sorted(by:{$0.firstName < $1.firstName})
      return fnSorted
    }
  
  
    // method that uses "filter" to return only peple who are between
    // 25 and 40 years old
    func filterAge() -> [Person]
    {
      let filteredAge = people.filter{Int($0.birthDay)! >= 1972
        && Int($0.birthDay)! <= 1991}
      return filteredAge
    }
  
//     methods that returns a tuple containing the  youngest and oldest
//     persons in the project team
    func maxMinAge() -> (youngest:String, oldest:String)
    {
      let youngest = people.max(by: {Int($0.birthDay)! > Int($1.birthDay)!})
      let oldest = people.min(by: {Int($0.birthDay)! < Int($1.birthDay)!})
      
      return (youngest!.description, oldest!.description)
      
      
    }
  
    // method that uses "reduce" to return number of people
    //younger than 22 years
    func reduce22() -> Int
    {
      
      func sum (accumulator: Int, current: Person) -> Int {
        var count = accumulator
        if Int(current.birthDay)! > 1994 {
        count += 1
        
        }
        return count
      }
     
      let total = people.reduce(0, sum)
      
      return total
  
  
    }
      
  
  
    // method that uses "reduce" to return the average age of the team
    func reduceAverageAge() -> Int
    {
      func average (accumulator: Int, current: Person) -> Int {
        var count = accumulator
        if Int(current.birthDay)! > 1994 {
          count += Int(current.birthDay)!
          
        }
      
      let averageTotal = people.reduce(0, average)
      
      return averageTotal
    }
  
   // method that uses "reduce" to return the average length of the last
   // names in the team (number of characters)
    func lastNameAverage() -> Int
    {
      func length (accumulator: Int, current: Person) -> Int{
        var count = accumulator
         let countLength = current.lastName.characters.count
        count = count + countLength
          
        }
      
      let nameAverage = people.reduce(0, length)
      return nameAverage/100
      

     
    }
  
  
}
