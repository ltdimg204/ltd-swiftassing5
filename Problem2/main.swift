//
//  main.swift
//  Problem2
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

//
//  main.swift
//  assign-5
//
//  Created by Frank  Niscak on 2016-10-11.
//  Copyright © 2016. All rights reserved.
//
// Project:      assign-5-2016
// File:         Problem2.swift
// Author:       Thais Lescano
// Date:         Sep 14, 2016
// Course:       IMG204

// Problem Statement:
// Complete the provided Person and Project classes. Also, use the
// provided Generator class to create 100 people assigned to a
// project.
//
// Study and modify (if needed) the provided "main.swift" file.
// Your task is to complete both the Person and Project classes so that the
// "main.swift" runs without errors.
//
//
// Inputs: none
// Outputs: results of all methods in an object created from the Project class.




// create an object from the Project class
let myProject:Project = Project()

// use the Generator class to create array of Persons
let HOW_MANY: Int = 100

let strArray = Generator.generateMultiplePersonRecords(howMany: HOW_MANY)
for aString in strArray {
  var strTmp = aString.components(separatedBy: "|")
  let tmpPerson = Person(firstName: strTmp[1], lastName: strTmp[2],
                         birthDay: strTmp[3], city: strTmp[4],
                         province: strTmp[6])
  myProject.addPerson(person1:tmpPerson)
}


//print out data inside myProject
print(myProject.description)

//using for to display results
print("\nSort by age descending")
for ageDesc in myProject.sortAgeDesc()
{
  print(ageDesc.description)
}

print("\nSort by Name ascending")
for nameAsc in myProject.sortFirstName()
{
  print(nameAsc.description)
}

print("\nFilter Age between 25 and 40 Years Old")
for age in myProject.filterAge()
{
  print(age.description)
}

print("\nCreate A Tuple")
print(myProject.maxMinAge())

print("\nTotal Under 22: \(myProject.reduce22())")

let averAge = 2016 - myProject.reduceAverageAge()
print("\nAverage Age: \(averAge)")


print("\nAverage Last Name Size: \(myProject.lastNameAverage())")

