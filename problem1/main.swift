//
//  main.swift
//  problem1
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

//  main.swift
//  assign-5
//
//  Created by Frank  Niscak on 2016-10-11.
//  Copyright © 2016. All rights reserved.

// Project:      assign-5-2016
// File:         Problem1.swift
// Author:       Thais
// Date:         Sep 14, 2016
// Course:       IMG204

// Problem Statement - Class Inheritance :
//
// Design and implement a set of classes that define various types of reading
// material: books, novels, magazines, technical journals, textbooks, and so
// on. Be sure that appropriate classes are created by using class inheritance.
//
// Include data values that describe various attributes (properties) of the
// material, such as the number of pages and the names of the primary characters.
//
// Include appropriate overridden methods in your classes so that you can print
// relevant information about each object. Your main.swift should instantiate
// and test all methods of at least one object of each class.
//
// Inputs:   none
// Outputs:  information about each object created in main.swift

import Foundation


let book1 = Book(author: "Chimamanda Adichie", title: "Americanah",
                 publisher: "Rocco",
                 isbn: "98678", edition: "3ed", genre: "Drama")

print (book1.description)

let article1 = Article(author: "Joe Moe", title: "Data mining",
                       website: "www.articlesonline.com/datamining",
                       subject: "computer science", publishDate: "2016-07-14")

print(article1.description)

let magazine1 = Magazine(author: "Wilma Colins", title: "Beauty queen",
                         publisher:"new magazines",
                         isbn: "85854",
                         editionMonth: "January", year:"2016")
print(magazine1.description)

let blog1 = Blog(author: "Thais Lescano", title: "my dreams",
                 website: "www.mydreams.com",
                 target: "teenagers", hasApp: false)
print (blog1.description)

print (blog1.getTitle())
print (book1.getAuthor())
print (article1.getWeb())
