//
//  Blog.swift
//  ltd-assing5
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Blog : Online {
  let target: String
  let hasApp: Bool
  
  init(author: String, title: String, website: String,
       target: String, hasApp: Bool) {
    self.target = target
    self.hasApp = hasApp
    super.init(author: author, title: title, website: website)
  }
  
  override var description: String {
    let thisdescrip = "target: \(target). Has an app? \(hasApp)"
    return super.description + thisdescrip
    
  }
}
