//
//  Online.swift
//  ltd-assing5
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

import Foundation

class Online : ReadingMaterial {
  let website: String
  
  init(author: String, title: String, website: String) {
    self.website = website
    super.init(author: author, title: title)
  }
  
  override var description: String {
    let thisdescrip = "You can find it at \(website)"
    return super.description + thisdescrip
    
    
  }
  
  func getWeb () -> String {
    
    return self.website
  }
}
