//
//  ReadingMaterial.swift
//  ltd-assing5
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

import Foundation

class ReadingMaterial :CustomStringConvertible {
  let author: String
  let title: String
  
  init(author: String, title: String) {
    
    self.author = author
    self.title = title
    
    
  }
  
  func getTitle() -> String {
    return self.title
    
  }
  func getAuthor () -> String {
    return self.author
    
  }
  
  var description: String {
    return "\(title) from \(author)"
  }
  
  
  
  
  
}
