//
//  Book.swift
//  ltd-assing5
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Book: Paper {
  let edition: String
  let genre: String
  
  init(author: String, title: String, publisher: String, isbn: String,
       edition: String, genre: String) {
    self.edition=edition
    self.genre = genre
    super.init(author: author, title: title, publisher: publisher, isbn:isbn)
  }
  
  override var description: String {
    
    let thisdescrip = "edition: \(edition), genre: \(genre)"
    return super.description + thisdescrip
    
  }
  
}
