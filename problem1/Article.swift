//
//  Article.swift
//  ltd-assing5
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Article: Online {
  let subject : String
  let publishDate: String
  
  init(author: String, title: String, website: String,
       subject: String, publishDate: String) {
    self.subject = subject
    self.publishDate = publishDate
    super.init(author: author, title: title, website: website)
  }
  
  override var description : String {
    let thisdescrip = "Article's subject:\(subject)pusblishDate: \(publishDate)"
    return super.description + thisdescrip
    
  }
  
  
}
