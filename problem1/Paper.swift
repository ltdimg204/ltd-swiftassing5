//
//  Paper.swift
//  ltd-assing5
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Paper : ReadingMaterial {
  let publisher: String
  let isbn : String
  
  init(author: String, title: String, publisher: String, isbn: String){
    self.publisher = publisher
    self.isbn = isbn
    super.init(author: author, title: title)
    
  }
  
  override var description: String {
    let thisdescrip = "publisher: \(publisher), isbn: \(isbn)"
    return super.description + thisdescrip
    
  }
  
}
