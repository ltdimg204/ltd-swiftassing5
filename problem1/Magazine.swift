//
//  Magazine.swift
//  ltd-assing5
//
//  Created by Student on 2016-10-25.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

import Foundation

class Magazine: Paper {
  let editionMonth : String
  let year: String
  init(author: String, title: String, publisher: String, isbn: String,
       editionMonth: String, year: String) {
    self.editionMonth = editionMonth
    self.year = year
    super.init(author: author, title: title, publisher: publisher,
               isbn: isbn)
    
  }
  
  override var description: String {
    let thisdescrip = "Month of edition: \(editionMonth), year: \(year)"
    return super.description + thisdescrip
    
  }
  
}
